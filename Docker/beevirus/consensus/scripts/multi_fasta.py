#!/usr/bin/env python3
import argparse
import re
import sys
import collections
import operator
import random
from itertools import chain
import os.path
import time

"""
Berekent een consensus sequentie door eerst een dictionary te maken met elk van de letters met de bij behorende positie 
als key met als value het aantal keer dat die letter op die positie voorkomt. Dan kun je de nucleotide gaan tellen per 
sequentie op die plek. Als er bijvoorbeeld twee nucleotide even vaak voorkomen dan wordt er eerst gekeken of de 
ambiguity letters maar een van de twee letters kan zijn zo ja dan wordt de letter met wie hij kan matchen gebruikt als 
nucleotide voor die plek, zo niet dan wordt er random gekozen tussen die twee nucleotiden. Als er meer gaps in voorkomen
dan letters of de getelde letter een coverage van 1 hebben op die positie dan wordt er niks van die positie van de msa 
in de consensus sequentie gedaan. De informatie over de consensus komen in een apart mapje terecht.
"""

input_file = "/students/2019-2020//Thema11/minion_js_jh/msa/data/all_world.fasta"
output_file = "/students/2019-2020/Thema11/minion_js_jh/msa/msa_output/world_default/world_default.fasta"
accession_together = "MK262742 MK262743"

print("default removing = {}".format(accession_together))
inpt = input("want to remove your own accession:")
if inpt == "yes" or inpt == "y" or input == "Y" or inpt == "Yes":
    accession_together += " {}".format(input("put your accesion here:"))
else:
    print("Default accessions are being used.")

print("{} names are being removed".format(accession_together))

accession = accession_together.split(" ")


class Consensus:

    def __init__(self):
        """
        Krijgt een input_file naam, output_file naam, perfectlength, count, report_file naam mee.
        Als de meegegeven counts percentage niet hoger is dan 50% dan wordt de default waarde van counts gepakt.
        """

        args = get_file_name()

        self.input_file = args.inputfile
        self.output_file = args.outputfile
        self.accession = args.accession
        if type(self.accession) == list:
            print('true')
        else:
            self.accession = [self.accession]


    def sequence_parser(self):
        """
        Opent de input file en haalt de headers eruit en maakt een list van de gebruikte sequenties aan.
        :return: een lijst van Sequenties
        """
        sequence = ''
        op_file = open("{}".format(self.input_file))
        for line in op_file:
            if line.startswith(">"):
                line = line.replace("Deformed wing virus ", "DWV ")
                line = line.replace("| complete genome", "")
                print(line)
                line = line.replace(">", "#>")
                sequence += line
            else:
                sequence += line
        sequences = sequence.split("#")

        return sequences

    def check_files(self):
        """
        Kijkt of de report outfiles al bestaan. Zo ja dan worden ze verwijderd.
        :return:
        """
        if os.path.isfile("{}".format(self.output_file)):
            print("{}".format(self.output_file))
            os.remove("{}".format(self.output_file))

    def accession_remover(self, sequences, pipe_line):
        """
        Hij haaltde poly-A staarten weg en telt per sequentie de nucleotides.
        :param sequences: Lijst met elke sequentie.
        :param pipeline: obje
        :return: een dictionary met de getelde nucleotide op elke positie.
        """
        index = 0
        fasta = ""

        for access in self.accession:
            for seq in sequences:
                if seq.startswith(">" + access):
                    print("removing sequences with this accession.")
                    print(access)
                    del sequences[index]
                index += 1
            index = 0

        for seq in sequences:
            fasta += seq

        pipe_line.report_output_file(fasta)

        return sequences

    def report_output_file(self, report):
        """

        :param report: Is de zin die aan de file wordt toegevoegd
        :param name: Zegt in welke file hij moet schrijven.
        :return:
        """
        # kijkt of de file al is aangemaakt. zo ja dan voegt hij de report als zin eraan toe,
        # zo niet dan maakt hij de file aan en voegt de report eraan toe.
        if os.path.isfile("{}".format(self.output_file)):
            print("print this shouldnt happen")
            with open("{}".format(self.output_file), 'a') as file:
                file.write('{}'.format(report))
                file.close()
        else:
            print("making file")
            with open("{}".format(self.output_file), "w+") as file:
                file.write(report)
                file.close()



def get_file_name():
    """
    hier kun je de input file naam, output file naam, report file naam, lengte van accurate sequentie en het aantal
    percentage van sequentie dat dezelfde nucleotide had gekozen.
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--inputfile", default=input_file,
                        help="insert the filename of the input file must be a msa output .fasta file.")
    parser.add_argument("--outputfile", default=output_file,
                        help="insert the filename of the output file. It is a fasta so must end with .fa or .fasta")
    parser.add_argument("--accession", default=accession,
                        help="insert the directory for the report files.")


    args = parser.parse_args()
    return args


def main():
    """
    Voert alles op volgorde uit en schrijft naar de Info_bout_consenus file hoelang het duurde om alles te hebben
    uitgevoerd.g
    :return:
    """
    # uitvoeren

    pipe_line = Consensus()
    pipe_line.check_files()
    seq = pipe_line.sequence_parser()
    pipe_line.accession_remover(seq, pipe_line)
    return 0


if __name__ == '__main__':
    sys.exit(main())
