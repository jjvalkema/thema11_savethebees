#!/usr/bin/env python3
from typing import Any

import argparse
import re
import sys
import collections
import operator
import random
from itertools import chain
import os.path
import time

"""
Berekent een consensus sequentie door eerst een dictionary te maken met elk van de letters met de bij behorende positie 
als key met als value het aantal keer dat die letter op die positie voorkomt. Dan kun je de nucleotide gaan tellen per 
sequentie op die plek. Als er bijvoorbeeld twee nucleotide even vaak voorkomen dan wordt er eerst gekeken of de 
ambiguity letters maar een van de twee letters kan zijn zo ja dan wordt de letter met wie hij kan matchen gebruikt als 
nucleotide voor die plek, zo niet dan wordt er random gekozen tussen die twee nucleotiden. Als er meer gaps in voorkomen
dan letters of de getelde letter een coverage van 1 hebben op die positie dan wordt er niks van die positie van de msa 
in de consensus sequentie gedaan. De informatie over de consensus komen in een apart mapje terecht.
"""

#input_file = "/students/2019-2020//Thema11/minion_js_jh/msa/data/world_default.fasta"
input_file = "/students/2019-2020/Thema11/minion_js_jh/msa/msa_output/world3_output.fa"
output_file = "/students/2019-2020/Thema11/minion_js_jh/msa/msa_output/world_default/"


class Consensus:

    def __init__(self):
        """
        Krijgt een input_file naam, output_file naam, perfectlength, count, report_file naam mee.
        Als de meegegeven counts percentage niet hoger is dan 50% dan wordt de default waarde van counts gepakt.
        """

        args = get_file_name()
        self.input_file = args.inputfile
        self.output_file = args.outputfile
        self.res = {}
        self.names = []
        self.direc_values = 0
        self.gaps_on_pos = {}
        self.sequence_validate = {}
        self.percentage = 0
        self.sequence_length = 0

    def sequence_parser(self):
        """
        Opent de input file en haalt de headers eruit en maakt een list van de gebruikte sequenties aan.
        :return: een lijst van Sequenties
        """
        sequence = ''
        op_file = open("{}".format(self.input_file))
        for line in op_file:
            if line.startswith(">"):
                self.names.append(line)
                line = line.replace(line, "#")
                sequence += line
            else:
                sequence += line
        sequence = sequence.replace("\n", "")
        sequences = sequence.split("#")
        self.sequence_length = len(sequences[1])

        return sequences

    def check_files(self):
        """
        Kijkt of de report outfiles al bestaan. Zo ja dan worden ze verwijderd.
        :return:
        """
        if os.path.isfile("{}{}".format(self.output_file, "Pos_gaps.csv")):
            print("removing Pos_gaps.csv file")
            os.remove("{}Pos_gaps.csv".format(self.output_file))

        if os.path.isfile("{}{}".format(self.output_file, "Accession_gaps.csv")):
            print("removing Accession_gaps.csv file")
            os.remove("{}Accession_gaps.csv".format(self.output_file))


    def nuc_count(self, sequences):
        """
        Hij haaltde poly-A staarten weg en telt per sequentie de nucleotides.
        :param sequences: Lijst met elke sequentie.
        :param pipeline: obje
        :return: een dictionary met de getelde nucleotide op elke positie.
        """

        for indx in range(len(self.names)):
            self.names[indx] = self.names[indx].split("|")[0]
            self.names[indx] = self.names[indx][1:-1]
        direc = {}
        for i in range(len(sequences[1])):
            direc["{}".format(i)] = 0

        print(sequences[0])
        del sequences[0]
        for index in range(len(sequences)):
            pos = 0
            for a in sequences[index]:
                """
                dictionary met elke posite de actg en de overige als keys en aantal als value.
                Als er characters in zitten dat dit script niet kan verwerken dan wordt dat geschreven naar de 
                info_about_consensus.txt
                """

                if a == "-":
                    direc["{}".format(pos)] += 1

                pos += 1

            value_sum = sum(direc.values())
            self.sequence_validate["{}".format(self.names[index])] = value_sum - self.direc_values
            self.direc_values = value_sum

        return direc


    def write_consensus(self, sequences, direc, pipeline):
        """
        :param sequences: lijst met sequenties
        :param direc: dictionary met getelde nucleotide op die positie.
        :param pipeline: class object voor het gebruik van classe functies
        :return:
        """
        header = ">gi|71480055|ref|NC_004830.2| Deformed wing virus, complete genome\n"

        # aantal sequenties gebruikt bij de MSA
        self.percentage = len(sequences) - 1

        for seq in sequences:
            pos = 0
            h = 0
            for a in seq:
                # maakt een tijdelijke dict aan met de count waardes per positie.
                self.res = {key: direc[key] for key in direc.keys() & {'{}'.format(pos)}}
                pipeline.calculate_threshold(pos)
                pos += 1

                h = 2
            # moet gaan breken als hij alle posities heeft gehad zodat hij dit niet nog 20 keer gaat uitvoern door de
            # for loop van seq in sequences.
            if h == 2:
                break

        print(self.sequence_validate)
        print(sum(self.gaps_on_pos.values()))

    def calculate_threshold(self, pos):
        self.gaps_on_pos["{}".format(pos)] = sum(self.res.values())

    def make_csv_files_1(self, pipe_line):
        report = "Position,Gaps,Gaps_encountered,color\n"
        pipe_line.report_output_file(report, "Pos_gaps.csv")
        report = ""
        print(self.sequence_length)
        for num in range(self.sequence_length):
            report += "{},".format(num)
            report += "{},".format(self.gaps_on_pos["{}".format(num)])
            if self.gaps_on_pos["{}".format(num)] is not 0:
                report += "{},".format(1)
                report += "red"
            else:
                report += "{},".format(0)
                report += "green"
            report += "\n"

            pipe_line.report_output_file(report, "Pos_gaps.csv")
            report = ""

    def make_csv_files_2(self, pipe_line):
        report = "accession,Gaps,color\n"
        pipe_line.report_output_file(report, "Accession_gaps.csv")
        report = ""
        for i in self.sequence_validate:
            report += "{},".format(i)
            report += "{},".format(self.sequence_validate["{}".format(i)])
            if self.sequence_validate["{}".format(i)] < self.sequence_length*0.05:
                report += "green"
            else:
                report += "red"

            report += "\n"

            pipe_line.report_output_file(report, "Accession_gaps.csv")
            report = ""

    def make_r_plots(self):
        return 0


    def report_output_file(self, report, name):
        """

        :param report: Is de zin die aan de file wordt toegevoegd
        :param name: Zegt in welke file hij moet schrijven.
        :return:
        """
        # kijkt of de file al is aangemaakt. zo ja dan voegt hij de report als zin eraan toe,
        # zo niet dan maakt hij de file aan en voegt de report eraan toe.
        if os.path.isfile("{}{}".format(self.output_file, name)):
            with open("{}{}".format(self.output_file, name), 'a') as file:
                file.write('{}'.format(report))
                file.close()
        else:
            with open("{}{}".format(self.output_file, name), "w+") as file:
                file.write(report)
                file.close()



def get_file_name():
    """
    hier kun je de input file naam, output file naam, report file naam, lengte van accurate sequentie en het aantal
    percentage van sequentie dat dezelfde nucleotide had gekozen.
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--inputfile", default=input_file,
                        help="insert the filename of the input file must be a msa output .fasta file.")
    parser.add_argument("--outputfile", default=output_file,
                        help="insert the filename of the output file. It is a fasta so must end with .fa or .fasta")


    args = parser.parse_args()
    return args


def main():
    """
    Voert alles op volgorde uit en schrijft naar de Info_bout_consenus file hoelang het duurde om alles te hebben
    uitgevoerd.g
    :return:
    """
    # uitvoeren

    pipe_line = Consensus()
    pipe_line.check_files()
    seq = pipe_line.sequence_parser()
    direc = pipe_line.nuc_count(seq)
    pipe_line.write_consensus(seq, direc, pipe_line)
    print("writing pos_gaps csv_file")
    pipe_line.make_csv_files_1(pipe_line)
    print("writing accession_gaps csv_file")
    pipe_line.make_csv_files_2(pipe_line)

    pipe_line.make_r_plots()


    return 0


if __name__ == '__main__':
    sys.exit(main())
