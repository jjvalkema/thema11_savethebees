# Minor Project: Basecalling deformed wing virus data with taiyaki

![Version](https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0.html)

> This repository provides a pipeline for basecalling with taiyaki.

## Table of contents

1. Introduction
2. Dependencies
3. Installation
4. Usage & Features
5. Suggestions for Future Updates
6. Bugs
7. Contact
8. License

## Introduction
---

In recent years, the populations of the Western honeybee (Apis mellifera) have been declining drastically.  One of the main causes is the increase of bees infected with the Deformed wing virus (DWV; Iflaviridae). When this virus enters a host, it will change the structure of the wings. Bees infected with this virus are not able to fly because their wings are shriveled. The virus is spread by the Varroa destructor mite. The data for this project was sequenced using the Oxford Nanopore Technologies MinION sequencer and an R9 flow cell. This sequencer produces long reads with a relatively high accuracy at a low cost. Long reads are extremely valuable because they provide information on how distal sequences are spatially related. However, the MinION sequencer has a notable error rate, somewhere between the five and fifteen percent. The MinION sequencer contains a R9 flow cell with proteins, also called pores. Genetic data flows through the pore and measures an electric signal. The strength of the signal is based on the nucleotides. MinION reads need to be base called in order to know the nucleotide sequence. The aim of this study is to counter the error rate of the MinION sequencer with a base-caller trained on the DWV data and to improve this base-calling model further. This training is done with a deep learning model. When this goal is achieved, we can answer questions about the mutation rate. 

## Dependencies
---

- A computer with GPU is neccessary for training with taiyaki.

## Installation
---

We would like to create docker containers that run the pipeline [automatically], but that is a goal for the future. These docker containers will have all the neccesarry tools installed, which makes the installation and use very easy. For now, the tools have to be installed individualy.

> It is recommended to install the tools in a self-contained [virtual environment](https://docs.python.org/3/tutorial/venv.html).

#### Install Guppy

Guppy is a basecalling software from Oxford NAnopore Technologies. Install it with the following commands:

```sh
# Download and unzip the compressed Guppy repository from Oxford Nanopore Technologies.
wget https://mirror.oxfordnanoportal.com/software/analysis/ont-guppy_3.4.4_linux64.tar.gz | tar zxvf ont-guppy_3.4.4_linux64.tar.gz
```
#### Install minimap2

Minimap2 is a versatile sequence alignment program that aligns DNA or RNA sequences against a large reference sequence. It performs especially well on long reads, for example reads from either PacBio or Minion Oxford Nanopore Technologies.

```sh
# Download and unzip the compressed github repository.
curl -L https://github.com/lh3/minimap2/releases/download/v2.17/minimap2-2.17_x64-linux.tar.bz2 | tar -jxvf -
./minimap2-2.17_x64-linux/minimap2
```

#### Install taiyaki

Taiyaki can be installed by cloning their repository. Look at the [taiyaki github](https://github.com/nanoporetech/taiyaki) for other ways to install taiyaki.

```bash
# Use pip to install taiyaki in a virtual env
pip install path/to/taiyaki/repo
pip install -e path/to/taiyaki/repo #[development mode](http://setuptools.readthedocs.io/en/latest/setuptools.html#development-mode)
```

#### Install Snakemake

The pipeline is written with snakemake. Install snakemake to execute the pipeline.

```sh
# Install snakemake
pip install snakemake
```

It is not possible to create a pipeline image if dot is not installed. When running the snakemake --dag command and dot is not installed, it will return a Broken Pipe error. Please install graphviz with the following command if this happens.

```sh
# Install graphviz
apt-get install graphviz
```

#### Install bwa - Burrows-Wheeler Alignment Tool
bwa is a tool that indexes and aligns fasta files.
To install bwa:

```sh
    git clone https://github.com/lh3/bwa.git
    cd bwa; make
    ./bwa index ref.fa
    ./bwa mem ref.fa read-se.fq.gz | gzip -3 > aln-se.sam.gz
    ./bwa mem ref.fa read1.fq read2.fq | gzip -3 > aln-pe.sam.gz
```


#### Install Nanoplot

Nanoplot is a tool that creates plots based on a basecalling summary file. The plots visualize the read length and base quality of the different reads. Use the following command to install Nanoplot:

```sh
    pip install NanoPlot
```

#### Install Clustal Omega

Clustal Omega is a tool for creating a multiple sequence alignment. Use the following command to install Clustal Omega:

```sh
conda install -c bioconda clustalo
# OR
conda install -c bioconda/label/cf201901 clustalo
```

#### Install Nanopolish
Nanopolish is a tool that can be used to look at the behavior of basecalled reads and for improving the consensus sequence.
to install Nanopolis:

```sh
    git clone --recursive https://github.com/jts/nanopolish.git
    cd nanopolish
    make
```

## Usage and Features
---

### The Taiyaki Pipeline

When all the necessary tools are installed, the pipeline can be run with the following command:

```sh
# Execute taiyaki pipeline
snakemake --cores all
# --cores all specifies the number of cores to use, in this case all of the cores are used.
```

Each step of the pipeline is explained below. The pipeline is shown in Figure 1 and has the following steps:

1. Basecall (basecall)
2. Alignment (map_basecall)
3. Prepare mapped reads (create_scaling_params, extract_references & create_mapped_read_file)
4. Training (train_model & export_to_guppy)
5. Basecalling with the new model (basecall_with_new_model)
6. Evaluation (quality_check, plot_mapped_reads, & plot_training)

![alt text](https://trello-attachments.s3.amazonaws.com/5e415bfb05c887724b30a712/5e81e00ac728c95a97a28d4c/b2f3deeae54545f2d5153983f33f22d5/image.png)

#### 1. Basecall with Guppy

The first step is base calling with Guppy and can be found in the pipeline as rule basecall. Guppy requires a directory of fast5 files and a configuration model. The configuration models can be found in data directory of the ont-guppy project. It has DNA and RNA configuration models for reads sequenced with R9.4.1 nanopores, each model has a high accuracy version and a fast version. The output is a directory containing the fastq files, two summary files and a log file.

```sh
guppy_basecaller -i reads -s basecalls -c rna_r9.4.1_120bps_hac.cfg --device cuda:0
```

| Options                         | Description                                   |
| --------------------------------|-----------------------------------------------|
| -i reads                        | An input directory with fast5 files           |
| -s basecalls                    | The output directory for the basecalled reads |
| -c rna_r9.4.1_120bps_hac.cfg    | The guppy configuration model, this one is specifically for RNA. One of the options in this configuration replaces the U with a T|
| --device cuda:0                 | Select one of the GPU's with CUDA              |

#### 2. Alignment with Minimap2

The generated fastq files from the previous step and the DWV reference genome acquired from NCBI are used by Minimap2 in rule map_basecall. This tool aligns each read to the reference genome to determine the specific reference fragment for each read. The resulting alignment in SAM format is converted to a BAM file with SAMtools. BAM files are the numeric version of SAM files and occupy less disk space.

```sh
 Minimap2 -I 16G -ax map-ont -t 32 -a --secondary=no reference.fasta basecalls/*.fastq | samtools view -b -S -T reference.fasta - > basecalls.bam
```

| Minimap Options   | Description                                   |
| ------------------|--------------------------------------------------------------------|
| -I 16G            | Only split the index every 16 gigabases                            |
| -ax map-ont       | Preset for mapping noisy RNA ONT reads to a reference              |
| -t 32             | Use 32 threads to run                                              |
| -a                | Output in SAM format                                               |
| --secondary=no    | Do not output secondary alignments                                 |
| reference.fasta   | Fasta file format containing the reference sequence to map against |
| basecalls/*fastq  | The fastq files output from the basecall                           |

#### 3. Prepare mapped reads

- ##### Extract References
 
    This step extracts the reference sequence for each read with the get_refs_from_sam.py script from Taiyaki. This step is executed by the rule extract_references. It requires alignment output from the previous step and the DWV reference genome. The reference sequences obtained with this script are used as the "true sequences" for the training. The default genetic data for Taiyaki is DNA. However, the data that is used for this project is RNA. During DNA sequencing, the strands of DNA go through the pore starting at the 5' end of the molecule. In contrast, during direct RNA sequencing the strands go through the pore starting at the 3' end. As a consequence, the per-read reference sequences used for RNA training must be reversed with respect to the genome/exome reference sequence. Therefore, the –reverse parameter is added.

```sh
    get_refs_from_sam.py reference.fasta  --reverse --min_coverage 0.8 > read_references.fasta
    # Add reverse when working with RNA
    # Select only reads with a coverage higher than 80%
```

- ##### Create scaling parameters
    
     This step selects how much each read is trimmed and it creates scaling parameters for each read with the generate_per_read_params.py script from taiyaki. This step is executed by the rule create_scaling_params. It uses the directory with the fast5 files to generate a tab separated file (tsv) with the following columns UUID (Read ID), trim_start, trim_end, shift and scale.

```sh
    generate_per_read_params.py --jobs 32 reads > read_params.tsv
    # Jobs is the number of threads
```

- ##### Create a mapped read file

    This step uses the sample directory with fast5 files, the base model and output from [create_scaling_params] and [extract_references]. This step is excecuted by the rule create_mapped_read_file. The base model is used to create an alignment between raw signal and reference sequence. The taiyaki script combines them into a single file, with for each read the parameters, the reference ("true sequence"), the raw signal and an alignment between the raw signal and the reference.  

```sh
    prepare_mapped_reads.py --jobs 32 reads read_params.tsv mapped_read.hdf5 r941_rna_minion.checkpoint read_references.fasta
```

| Options                    | Description                                         |
| -------------------------- | --------------------------------------------------- |
| reads                      | A directory containing the raw fast5 reads |
| read_params.tsv            | Tab seperated file with per read scaling and trimming parameters |
| mapped_read.hdf5           | The output, a hdf format file with the signal, reference and parameters for each read |
| r941_rna_minion.checkpoint | The model that aligns the raw signal with the reference |
| read_references.fasta      | The reference sequence per read |

#### 4. Training

- ##### Train a model

The progress is displayed on the screen and written to a log file in the training directory. Checkpoints are regularly saved and training can be restarted from a checkpoint by replacing the model description file with the checkpoint file on the command line.

```sh
    train_flipflop.py --device cuda:0 --overwrite --stride 10 --winlen 31 --seed 2 r941_rna_minion.checkpoint mapped_reads.hdf5 --outdir training
```

| Options                    | Description                                         |
| -------------------------- | --------------------------------------------------- |
| --device cuda:0            | Select which GPU with cuda |
| --overwrite                | Overwrite the training if the output directory already exists |
| --stride 10                | The output, a hdf format file with the signal, reference and parameters for each read |
| --winlen 31                | The model that aligns the raw signal with the reference |
| --seed 2                   | The seed makes this project reproducible |
| r941_rna_minion.checkpoint | Model definition file |
| mapped_reads.hdf5          | A hdf format file with the signal, reference and parameters for each read |
| --outdir training          | The name of the directory where the checkpoint files and the resulting model wil be stored |

- ##### Export to guppy

Convert the final checkpoint file from the training into a json file with taiyaki script dump_json.py. Guppy requires json files for its basecalling.

```sh
    dump_json.py training/model_final.checkpoint > 'pwd'/model.json
```

#### 5. Basecall with new model

Basecall the files in the fast5 directory again but with the re-trained model. It is basically the same as step 1, but a model files is specified by the -m option.

```sh
guppy_basecaller -i samples -s basecalls -c rna_r9.4.1_120bps_hac.cfg -m 'pwd'/model.json --device 0
```

| Options                         | Description                                   |
| --------------------------------|-----------------------------------------------|
| -i reads                        | An input directory with fast5 files           |
| -s basecalls                    | The output directory for the basecalled reads |
| -c rna_r9.4.1_120bps_hac.cfg    | The guppy configuration model, this one is specifically for RNA. One of the options in this configuration replaces the U with a T|
| 'pwd'/model.json                | Use the retrained model to basecall           |
| --device cuda:0                 | Select one of the GPU's with CUDA             |

#### 6. Evaluation

- ##### Quality check with Nanoplot

    The quality and the read length of each read can be observed by Nanoplot. This tool creates a lot of different graphs to visualize the performance of the basecalling. 

```sh
    Nanoplot --summary basecalls/sequencing_summary.txt --loglength -o Nanoplot_output_basecalls
```

| Options                                    | Description                                   |
| ------------------------------------------ | --------------------------------------------- |
| --summary basecalls/sequencing_summary.txt | A text file format summary of the basecalling performance |
| --loglength                                | Create plots that are log transformed|
| -o Nanoplot_output_basecalls               | The output directory |

- ##### Plot mapped reads

    A mapped read file is created in step 3 prepare mapped reads. It contains the parameters, reference and singal per read. It also contains an alginment between the signal and the reference. This alignment is visualised in a mapped reads plot. The plot_mapped_singals.py script was written by taiyaki and can be found in the misc directory of taiyaki. Examples of the output can be found in the taiyaki/output/ directory on this repository.

```sh
    plot_mapped_singals.py mapped_reads.hdf5 --nreads 100 --output plot_mapped_reads.png

```

| Options                                    | Description                                   |
| ------------------------------------------ | --------------------------------------------- |
| --nreads 100                               | The number of reads used for the plot |
| --output plot_mapped_reads.png             | The output file with a png format |

- ##### Plot training

    The training is also visualized in a graph, it shows the training loss. The script to create this plot can be found in the taiyaki misc directory. Examples of the output can be found in the taiyaki/output/ directory on this repository.

```sh
    plot_training.py plot_training.png training
```

- ##### Create a workflow image

    This rule creates an image of the pipeline. An example of this is Figure 1 from this readme.

```sh
    snakemake --dag | dot Tpng > snakemake_pipleine.png
```


### Create a consensus sequence

The best way to validate your taiyaki for errors is to use a consensus sequence as reference sequence.
We have made a program that makes a consensus sequence.

The consensus python script removes the poly-a-tail and counts the different nucleotides at each position of every sequence used in the MSA. It removes the poly-a-tail because some sequences are shorter than others and with the poly-a-tail, it could form a bias towards the 'A' nucleotides at those positions. The program also examines whether the nucleotide with the highest frequency on a position occurs in more than one sequence from the MSA sequences. If the nucleotide for that position occurs in only one of the MSA sequences, the nucleotide is deemed unreliable and will be removed from the consensus sequence. Sometimes, there are multiple nucleotides with the same frequency top score on a position, for example the nucleotides A and G both have a frequency of 10 (in an MSA with 25 sequences). When this happens, the program searches for any non-nucleotide characters on that position in the MSA. The letter 'N' represents the nucleotides 'A' and 'C' and the letter 'R' represents the nucleotides 'A' and 'G'. When an 'N' is found, the 'A' nucleotide will appear in the consensus on that position. When an 'R' is found, the program will randomly choose between 'A' and 'G'. When there aren't any non-nucleotide characters while A and G have both the highest count on that position (10) the program will choose randomly between the two. Must have a coverage of atleast 2 nucleotides from the MSA for meeting the minimal requirements to be passed down to the consensus sequence. If the most frequent on a certain location is a non-nucleotde that represents three or more nucleotide will it be removed as most frequent. consensus sequence doesnt contain gaps.

The consensus program writes three output reports: 

- correct_sequences.txt:  When the nucleotide on a certain position is chosen by all sequences five or more times in a row. Contains the sequence with their the start and stop position and sequence length 

- Nuc_changes.txt: Each time the involvement of a non-nucleotide letter changes the outcome or doesnt change the outcome, if there are more nucleotides with the same top frequency on that position and when it has to choose randomly between the letters.

- Info_bout_consensus.txt: Contains how long the MSA was, how many nucleotides all sequences agree on on the same position, how many times nothing was added to the consensus sequence because it didnt met the requirements, the length of the consensus sequence, the frequency of coverage for every nucleotide chosen for the consensus sequence between 0 and 100% in per 10%, the percetage of the consensus sequence that has a coverage of more than 50%, the characters that the script can't handle.


To execute the program you can type:

```sh
python3 oo_consensus.py --inputfile --outputfile --reportfile --perfectlength --counts
```

| Options                                    | Description                       | Mandatory  
| ------------------------------------------ | ----------------------------------|---------- |
| --inputfile        msa_output.fasta        | The pathway to the multiple sequence alignment output in fasta format | Yes|
| --outputfile       eigen_consensus.fasta   | The pathway to a outputfile location must be .fasta | Yes|
| --reportfile        reports/               | The pathway to a directory for reportfiles. Three output files will be created into this directory | Yes|
| --perfectlength       5                    | Insert the minimal length of the sequence for your report file for consecutive nucleotdes where (self given percentage --counts) of the sequences from the msa chose the same nucleotide. Has a default of 5. | No|
| --counts           100                     | Insert percentage in % for how many you want to be correct to be called for in your report. Must be higher than 50%. Has a default of 100%. | No|

## Suggestions for future updates
---

- Use docker to create a fully automated pipeline.
- Create a configuration file for the taiyaky pipeline.
- Distribute the taiyaki pipeline tasks between CPU and GPU, to increase the speed.
- Implementing msa/consensus and Nanopolish steps in one snakemake pipeline.
- Make a barplot with oo_consensus.py which contains the frequency for 0-10%, 10-20%, ... etc, until 100%

## Bugs
---

## Contacts
---

👤 **Jippe Silvius & Jildou Haaijer**

For any problems or questions:

📧 **j.d.silvius@st.hanze.nl & j.f.haaijer@st.hanze.nl**

## 📝 Licenses
---

This project is [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) licensed.
