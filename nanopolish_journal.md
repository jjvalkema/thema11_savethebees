# canu draft genome (errors)  

source: https://nanopolish.readthedocs.io/en/latest/quickstart_consensus.html  
canu draft genome assembly https://github.com/marbl/canu  
``source /homes/jjvalkema/Desktop/thema11_savethebees/venv/bin/activate``

------------------------------------------------------------
create new output folder (empty if exist)    
``rm -R -f /students/2019-2020/Thema11/minion_aj_jj/data/draft_genome_assembly_canu; mkdir /students/2019-2020/Thema11/minion_aj_jj/data/draft_genome_assembly_canu; cd /students/2019-2020/Thema11/minion_aj_jj/data/draft_genome_assembly_canu``  

aggregate all reads      
``cat $(find $PWD /students/2019-2020/Thema11/minion_js_jh/taiyaki_output/basecalls_final | egrep '\.fastq$' | tr '\n' ' ') > temp_all_reads.fastq``  
  
  
convert them to dna  
``python3 /students/2019-2020/Thema11/minion_aj_jj/programs/thema11_savethebees/scripts/python/fastq_nucleotide_converter.py -i /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/temp_all_reads.fastq -o /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/temp_all_reads_dna.fastq --rna``  

------------------------------------------------------------

### attempt 1   
settings as described at "Compute the draft genome": https://nanopolish.readthedocs.io/en/latest/quickstart_consensus.html    
ERROR scontrol: error: s_p_parse_file: unable to status file /etc/slurm-llnl/slurm.conf: No such file or directory, retrying in 1sec up to 60sec    
full stdout at:  ``/students/2019-2020/Thema11/minion_aj_jj/data/errors_logs/canu_run_1    ``  
``canu -p deformed-wing-virus -d /students/2019-2020/Thema11/minion_aj_jj/data/draft_genome_assembly_canu genomeSize=10140 -nanopore-raw /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/temp_all_reads.fastq``  

### attempt 2   
settings as 1 with useGrid=false    
from https://canu.readthedocs.io/en/latest/tutorial.html#execution-configuration      
``canu -p deformed-wing-virus -d /students/2019-2020/Thema11/minion_aj_jj/data/draft_genome_assembly_canu genomeSize=10140 -nanopore-raw /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/temp_all_reads.fastq useGrid=false``    
error:  canu iteration count too high, stopping pipeline (most likely a problem in the grid-based computes)    
full stdout at: ``cat /students/2019-2020/Thema11/minion_aj_jj/data/errors_logs/canu_run_2``   

### attempt 3  
settings as 2 just ran again lol  
from https://canu.readthedocs.io/en/latest/tutorial.html#execution-configuration    
``canu -p deformed-wing-virus -d /students/2019-2020/Thema11/minion_aj_jj/data/draft_genome_assembly_canu genomeSize=10140 -nanopore-raw /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/temp_all_reads.fastq useGrid=false``    
error:  canu iteration count too high, stopping pipeline (most likely a problem in the grid-based computes)   
full stdout at: ``/students/2019-2020/Thema11/minion_aj_jj/data/errors_logs/canu_run_3 ``   


### attempt 4    
settings as 2 just but with converted to dna  
from https://canu.readthedocs.io/en/latest/tutorial.html#execution-configuration  
``canu -p deformed-wing-virus -d /students/2019-2020/Thema11/minion_aj_jj/data/draft_genome_assembly_canu genomeSize=10140 -nanopore-raw /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/temp_all_reads_dna.fastq useGrid=false``   
error:  canu iteration count too high, stopping pipeline (most likely a problem in the grid-based computes)    
full stdout at: ``/students/2019-2020/Thema11/minion_aj_jj/data/errors_logs/canu_run_4   `` 
--------------------------------------------------------------
# wtdbg2 draft assembly (error)     
scource: https://nanopolish.readthedocs.io/en/latest/quickstart_consensus.html#compute-the-draft-genome-assembly-using-canu   
  
### install  
minimap2  
Go to install folder  
``cd /students/2019-2020/Thema11/minion_aj_jj/programs``  
Clone  
``git clone https://github.com/lh3/minimap2``    

bwa
``suo apt-get install bwa``

samtools  
``sudo apt-get install samtools``

biopython
``pip3 install biopython``

wtdbg2  
Go to install folder
``cd /students/2019-2020/Thema11/minion_aj_jj/programs``  
Clone
``git clone https://github.com/ruanjue/wtdbg2``    
--------------------
create new output folder (empty if exist)    
``rm -R -f /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly; mkdir /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly; cd /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly``    

aggregate all reads  
``cat $(find $PWD /students/2019-2020/Thema11/minion_js_jh/taiyaki_output/basecalls_final | egrep '\.fastq$' | tr '\n' ' ') > temp_all_reads.fastq``  

Align the original reads to draft assembly  
``/students/2019-2020/Thema11/minion_aj_jj/programs/minimap2/minimap2 -ax map-ont -t 8 draft.fa /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/temp_all_reads.fastq | samtools sort -o reads.sorted.bam -T reads.tmp  
samtools index reads.sorted.bam``   
## run wtdbg2    

### attempt 1  
assemble  
``/students/2019-2020/Thema11/minion_aj_jj/programs/wtdbg2/wtdbg2 -x ont -g 10140 -t 16 -fo DWV -i temp_all_reads.fastq``   
   
consensus (TODO ERROR HAS NO U NUCLEOTIDE??)      
``/students/2019-2020/Thema11/minion_aj_jj/programs/wtdbg2/wtpoa-cns -t 16 -i DWV.ctg.lay.gz -fo DWV.ctg.fa``     

stdout at: ``/students/2019-2020/Thema11/minion_aj_jj/data/errors_logs/wtbg2_attempt_1 ``   

### attempt 2     
run wtdbg2 but convert to dna    
convert them to dna    
``python3 /students/2019-2020/Thema11/minion_aj_jj/programs/thema11_savethebees/scripts/python/fastq_nucleotide_converter.py -i /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/temp_all_reads.fastq -o /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/temp_all_reads_dna.fastq --rna``    
  
assemble  
``/students/2019-2020/Thema11/minion_aj_jj/programs/wtdbg2/wtdbg2 -x ont -g 10140 -t 16 -fo DWV -i temp_all_reads_dna.fastq``   
   
consensus (ERROR NO OUTPUT??)      
``/students/2019-2020/Thema11/minion_aj_jj/programs/wtdbg2/wtpoa-cns -t 16 -i DWV.ctg.lay.gz -fo DWV.ctg.fa``  
  
stdout at: ``/students/2019-2020/Thema11/minion_aj_jj/data/errors_logs/wtbg2_attempt_2_dna``     
``python3 /students/2019-2020/Thema11/minion_aj_jj/programs/thema11_savethebees/scripts/python/fastq_nucleotide_converter.py -i temp_all_reads.fastq -o temp_all_reads_dna.fastq --rna  ``   
  
--------------------
  
    
# nanopolish / create polished consensus (ERROR)
### install nanopolish  
connect  
``ssh assemblix``  

build from github  
``cd /students/2019-2020/Thema11/minion_aj_jj/programs ``   
``git clone --recursive https://github.com/jts/nanopolish.git``  
``cd nanopolish  ``  
``make``    

python packages for the scripts (do from venv!!!!)   
``pip install Bio``    
--------------------  
  
<!--
preproccess (nanopolish index -d /path/to/raw_fast5s/ -s sequencing_summary.txt albacore_output.fastq)     
/students/2019-2020/Thema11/minion_aj_jj/programs/nanopolish/nanopolish index -d /students/2019-2020/Thema11/minion_aj_jj/pipeline/dwv_multi_reads/ -s /students/2019-2020/Thema11/minion_js_jh/taiyaki_output/basecalls_final/sequencing_summary.txt  /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/temp_all_reads.fastq    
-s /students/2019-2020/Thema11/minion_js_jh/taiyaki_output/basecalls_final/sequencing_summary.txt 1 file is excluded so you cant run with this file at least that is my assumption.    
-->

## polishing  
### Attempt 1 wtdbg2 draft genome assembly (the one without u nucleotide) (0.13.0 )
From readme TODO link    
change dir for output  
``cd /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly``   

create temp combined fastq  
``cat $(find $PWD /students/2019-2020/Thema11/minion_js_jh/taiyaki_output/basecalls_final | egrep '\.fastq$' | tr '\n' ' ') > temp_all_reads.fastq``  

Index with bwa
``bwa index /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/DWV.ctg.fa``  
  
Align the basecalled reads to the draft sequence and index   
``cat $(find $PWD /students/2019-2020/Thema11/minion_js_jh/taiyaki_output/basecalls_final | egrep '\.fastq$' | tr '\n' ' ') > temp_all_reads.fastq``  
``bwa mem -x ont2d -t 8 /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/DWV.ctg.fa temp_all_reads.fastq | samtools sort -o reads.sorted.bam -T reads.tmp -``  
``samtools index reads.sorted.bam``  
 
Do indexing nanopolish      
``/students/2019-2020/Thema11/minion_aj_jj/programs/nanopolish/nanopolish index -d /students/2019-2020/Thema11/minion_js_jh/taiyaki_output/filtered_reads/ /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/temp_all_reads.fastq -s /students/2019-2020/Thema11/minion_js_jh/taiyaki_output/basecalls_final/sequencing_summary.txt``  

Do polish (can take a while)  
``
python3 /students/2019-2020/Thema11/minion_aj_jj/programs/nanopolish/scripts/nanopolish_makerange.py /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/DWV.ctg.fa | parallel --results nanopolish.results -P 80 /students/2019-2020/Thema11/minion_aj_jj/programs/nanopolish/nanopolish variants --consensus -o polished.{1}.vcf -w {1} -r temp_all_reads.fastq -b reads.sorted.bam -g /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/DWV.ctg.fa -t 80 --min-candidate-frequency 0.1
``    

 -P and -t are to set the thread count which set to 35 here  

TODO ERROR probably bad draft assembly from wtdbg2    
 error polishing after nanopolish index with sequencing_summary.txt nanopolish:    
 ``src/nanopolish_squiggle_read.cpp:114: void SquiggleRead::init(const string&, const Fast5Data&, uint32_t): Assertion `data.rt.n > 0' failed.  ``  
 ---------------------------------------
### Attempt 2 like 1 but without sequencing_summary.txt (0.13.0 ): 
Follow attempt 1 except do polish (nanopolish_makerange.py)  
   
Do polish (can take a while):  
``python3 /students/2019-2020/Thema11/minion_aj_jj/programs/nanopolish/scripts/nanopolish_makerange.py /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/DWV.ctg.fa | parallel --results nanopolish.results -P 35 /students/2019-2020/Thema11/minion_aj_jj/programs/nanopolish/nanopolish variants --consensus -o polished.{1}.vcf -w {1} -r /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/temp_all_reads.fastq -b /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/reads.sorted.bam -g /students/2019-2020/Thema11/minion_aj_jj/pipeline/draft_genome_assembly/DWV.ctg.fa -t 35 --min-candidate-frequency 0.1``  

 error without sequencing_summary.txt:  
 ``nanopolish: src/hmm/nanopolish_profile_hmm.cpp:36: float profile_hmm_score_set(const std::vector<HMMInputSequence>&, const HMMInputData&, uint32_t): Assertion `std::string(data.pore_model->pmalphabet->get_name()) == "nucleotide"' failed.``    
  
----------------------------------------
### Attempt 3 with reference genome instead of draft genome (cutting corners) (0.13.0 but same error on 0.13.2)         
Index the reference genome (bwa index draft.fa)      
``cd /students/2019-2020/Thema11/minion_aj_jj/data/nanopolish_output``  
  
Index with bwa   
``bwa index /students/2019-2020/Thema11/minion_js_jh/msa/consensus_sequence/world3_consensus.fasta``  
  
Align the basecalled reads to the refference   
``cat $(find $PWD /students/2019-2020/Thema11/minion_js_jh/taiyaki_output/basecalls_final | egrep '\.fastq$' | tr '\n' ' ') > temp_all_reads.fastq``    
``bwa mem -x ont2d -t 8 /students/2019-2020/Thema11/minion_js_jh/msa/consensus_sequence/world3_consensus.fasta temp_all_reads.fastq | samtools sort -o reads.sorted.bam -T reads.tmp -``    
``samtools index reads.sorted.bam``   

nanopolish index  
``/students/2019-2020/Thema11/minion_aj_jj/programs/nanopolish/nanopolish index -d /students/2019-2020/Thema11/minion_js_jh/taiyaki_output/filtered_reads/ temp_all_reads.fastq -s /students/2019-2020/Thema11/minion_js_jh/taiyaki_output/basecalls_final/sequencing_summary.txt``  

Do polish (can take a while) (ERROR)  
``python3 /students/2019-2020/Thema11/minion_aj_jj/programs/nanopolish/scripts/nanopolish_makerange.py \
/students/2019-2020/Thema11/minion_js_jh/msa/consensus_sequence/world3_consensus.fasta \
| parallel --results nanopolish.results -P 80 /students/2019-2020/Thema11/minion_aj_jj/programs/nanopolish/nanopolish variants --consensus -o polished.{1}.vcf -w {1} -r \ 
temp_all_reads.fastq -b reads.sorted.bam -g \
 /students/2019-2020/Thema11/minion_js_jh/msa/consensus_sequence/world3_consensus.fasta -t 80 --min-candidate-frequency 0.1``  

Error:  
``nanopolish: src/hmm/nanopolish_profile_hmm.cpp:36: float profile_hmm_score_set(const std::vector<HMMInputSequence>&, const HMMInputData&, uint32_t): Assertion `std::string(data.pore_model->pmalphabet->get_name()) == "nucleotide"' failed.
``  
TODO / future fix:    
Might be able to convert to dna in order to fix it but that might add more problems since the signal strength of the raw data might be different in rna.  
maybe find alternative like pilon or racon
--------------------------
 ### attempt 4 docker (Not yet done TODO)
 install  
 ``cd /students/2019-2020/Thema11/minion_aj_jj/programs ``   
``git clone --recursive https://github.com/jts/nanopolish.git; cd nanopolish; docker build .``    
 
 ````
docker run -v /path/to/local/data/data/:/data/ -it :image_id  \
./nanopolish eventalign -r /students/2019-2020/Thema11/minion_aj_jj/data/nanopolish_output/temp_all_reads.fastq  \ 
-b /students/2019-2020/Thema11/minion_aj_jj/data/nanopolish_output/reads.sorted.bam \ 
-g /students/2019-2020/Thema11/minion_js_jh/msa/consensus_sequence/world3_consensus.fasta
````
-------------
### Error building own machine
Build on debian:
````
No LSB modules are available.
Distributor ID:	Debian
Description:	Debian GNU/Linux 10 (buster)
Release:	10
Codename:	buster
````
Command args:
``git clone --recursive https://github.com/jts/nanopolish.git; cd nanopolish; make``

Error: 
````
g++ -o src/nanopolish_call_methylation.o -c -g -O3 -std=c++11 -fopenmp -fsigned-char -D_FILE_OFFSET_BITS=64  -I./include -I./htslib -I./minimap2 -I./fast5/include -I./src -I./src/hmm -I./src/thirdparty -I./src/thirdparty/scrappie -I./src/common -I./src/alignment -I./src/pore_model -I./src/io -I./eigen/ -fPIC src/nanopolish_call_methylation.cpp
src/nanopolish_call_methylation.cpp:25:10: fatal error: zlib.h: No such file or directory
 #include <zlib.h>
          ^~~~~~~~
compilation terminated.
make: *** [Makefile:155: src/nanopolish_call_methylation.o] Error 1
``````