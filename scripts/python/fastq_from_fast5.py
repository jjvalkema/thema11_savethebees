#!/usr/bin/env python
import sys
from argparse import ArgumentParser
import os.path
from os import path
import h5py

#todo docstring and clean up
class fastq_extractor:
    def __init__(self, fast5_folder_path, fastq_file_path):
        #TODO clearer error handeling for the user on these assertions
        assert path.exists(fast5_folder_path), "Inputfolder: {} doesn't exists!".format(fast5_folder_path)
        assert not path.exists(fastq_file_path), "Outputfile: {} already exists!".format(fastq_file_path)
        self.fast5_folder_path = fast5_folder_path
        self.fastq_file_path = fastq_file_path

    def extract_file(self, fast5_f_path):
        print("extracting: "+fast5_f_path)
        fast5_f = h5py.File(fast5_f_path, 'r')
        #TODO do multiprocessing
        for read in fast5_f.keys():
            fastq_data = fast5_f[read]['Analyses']['Basecall_1D_000']['BaseCalled_template']['Fastq']
            self.fastq_f.write(fastq_data.value.decode("utf-8"))
        fast5_f.close()

    def extract(self):
        self.fastq_f = open(self.fastq_file_path, "a+")

        for fast5_f_name in os.listdir(self.fast5_folder_path):
            if not (fast5_f_name.endswith(".txt") or fast5_f_name.startswith(".")):
                #check is it is fast5 TODO
                fast5_f_path = self.fast5_folder_path+"/"+fast5_f_name
                self.extract_file(fast5_f_path)

        self.fastq_f.close()


def main():
    parser = ArgumentParser("Tool for extracting the fastq data from a folder of fast5 files")
    parser.add_argument('-i', '--input_folder', required=True,
                        help="Path to input folder with the fast5 files containing fastq data")
    parser.add_argument('-o', '--output_file', required=True,
                        help="Path to output fastq file that will contain the extracted fastq data")
    args = parser.parse_args()

    extractor = fastq_extractor(fast5_folder_path=args.input, fastq_file_path=args.output)
    extractor.extract()

if __name__ == '__main__':
    main()