configfile: "Snakemake/config.json"


rule all:
    input:
        basecalls_new = config["output"]["basecalls_new"]


rule final_basecall:
    input:
        DWV_reads = config["input"]["raw_fast5_folder"],
        rna_model = config["model"]["rna_model"],
        json = config["model"]["json"]
    output:
        basecalls_new = config["output"]["basecalls_new"]
    shell:
        '/usr/local/ont-guppy/bin/guppy_basecaller -i {input.DWV_reads} -s {output.basecalls_new} -c {input.rna_model} -m {input.json} --device cuda:1'

rule dump_json:
    input:
        DWV_reads = config["input"]["raw_fast5_folder"],
        model_final = config["model"]["model_final"]
    output:
        json = config["model"]["json"]
    shell:
         '/lib/taiyaki/bin/dump_json {input.model_final} > {output.json}'

rule train:
    input:
        mapped_reads = config["output"]["mapped_reads"],
        train_flipflop = config["scripts"]["train_flipflop"],
    output:
        model_final = config["model"]["model_final"]
    shell:
        '/lib/taiyaki/bin/train_flipflop --device 0 --size 256 --stride 10 --winlen 31 /lib/taiyaki/models/mGru_flipflop {input.mapped_reads} --outdir {output.model_final}'

rule create_map:
    input:
        DWV_reads = config["input"]["raw_fast5_folder"],
        read_params = config["output"]["read_params"],
        read_references = config["output"]["read_references"],
        flip_flop_model = config["model"]["flip_flop_model"],
    output:
        mapped_reads = config["output"]["mapped_reads"]
    shell:
        '/lib/taiyaki/bin/prepare_mapped_reads  --jobs 32 --alphabet ACGT {input.DWV_reads} {input.read_params} {output.mapped_reads} {input.flip_flop_model} {input.read_references}'

rule scale_param:
    input:
        DWV_reads = config["input"]["raw_fast5_folder"],
    output:
        read_params = config["output"]["read_params"]
    shell:
        '/lib/taiyaki/bin/generate_per_read_params --jobs 32 {input.DWV_reads} > {output.read_params}'

rule get_refs:
    input:
        consensus = config["input"]["consensus"] ,
        basecalls_bam = config["output"]["basecalls_bam"],
    output:
        read_reference = config["output"]["read_references"]
    shell:
        '/lib/taiyaki/bin/get_refs_from_sam {input.consensus} {input.basecalls_bam} --reverse --min_coverage 0.8 > {output.read_reference}'

rule make_bam:
    input:
        consensus = config["input"]["consensus"],
    output:
        basecalls_bam = config["output"]["basecalls_bam"]
    shell:
         # TODO remove hardcoded file path "Snakemake/outputs/basecalls/*.fastq "
        '/usr/local/minimap2-2.17_x64-linux/minimap2 -I 16G -x map-ont -t 32 -a --secondary=no {input.consensus} Snakemake/outputs/basecalls/*.fastq | /usr/bin/samtools view -bST {input.consensus}  - > {output.basecalls_bam}'

rule guppy_basecaller:
    input:
        DWV_reads = config["input"]["raw_fast5_folder"],
        rna_model = config["model"]["rna_model"],
    output:
        basecalls = config["output"]["basecalls"]
    shell:
        '/usr/local/ont-guppy/bin/guppy_basecaller -i {input.DWV_reads} -s {output.basecalls} -c {input.rna_model} --device cuda:1'

