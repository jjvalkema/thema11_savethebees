# MinIon chiron RNA base caller for DWV #
## Project description ##
This project aims to retrain the machine learning model of the base caller for the minion sequencer for the derfomed 
wing virus (DWV). This is done for the base caller Guppy with Taiyaki to retrain on sequence data gathered from the DWV 
with both the MinIon sequencer and the Illumina sequencer. The Illumina sequences will be used as the models supervised 
data to learn how to interpret the MinIon sequences and base call them.  

<!--
TODO make seperate readme for training pipeline?
-->
## Pipeline  ##
This pipeline preprocess the data before training and creates a consensus sequence from reference sequences 
(from ncbi by default)   
  
It extracts DWV virus reads into a two folders. One as multi fast5 files and one as single fast5 files.  
It creates a consensus sequence as fasta file both with IUPAC ambiguity codes and without which are in the 
consensus_sequences folder.  
  
It also creates the .msf file from the multiple sequence alignment 
and the multifasta with the reference sequences downloaded from ncbi

 ![alt text](https://bitbucket.org/jjvalkema/thema11_savethebees/raw/d777025ac2b6ecf8144244a11b670691b38aa16e/pipeline/dag.svg)   
   
#### output folder structure
````
TODO update illumina
basefolder/
├── DWV_multi_reads_folder
|   └── All_multi_reads
├── DWV_single_reads_folder_merged
|   └── All_single_reads
└── consensus_sequences
    ├── DWV_reff_seqs_multifasta
    ├── DWV_reff_consensus_with_IUPAC.fasta
    ├── DWV_reff_consensus_without_IUPAC.fasta
    └── DWV_reff_MSA_fasta.msf

````

#### dependencies ####
git  
python3   
clustalO  
consambig (from EMBOSS package)

All dependencies inside the python venv are in dependencies_pip_freeze

## Configure and install the pipe line ####
### Install clustalO 
#### debian and ubunutu (apt):  
``sudo apt-get install clustalo``

#### Other/universal:     
Go to the clustalO website and download the latest precompiled binary:   
http://www.clustal.org/omega/#Documentation    
Go to the folder where it is downloaded and open a terminal there and move it to your bin as "clustalo" by doing:     
``sudo mv clustalo-* /usr/local/bin/clustalo``    

## Install consambig  
#### debian and ubunutu (apt): 
``sudo apt-get install emboss``  
  
#### Other/universal:   
Download:  
``wget ftp://emboss.open-bio.org/pub/EMBOSS/EMBOSS-6.6.0.tar.gz``  
Or browse your self to find the latest version:  
http://emboss.sourceforge.net/download/  
Extract and install:
````
tar -xf EMBOSS-6.6.0.tar.gz
cd EMBOSS-6.6.0
./configure; make; make install
````  
If you get a error about X11 you can install without it with this command:  
``./configure --without-x; make; make install``    

## Install fastqc
#### debian and ubunutu (apt): 
``sudo apt-get install fastqc``
#### Other/universal:   
TODO
https://raw.githubusercontent.com/s-andrews/FastQC/master/INSTALL.txt

## install trimmomatic
``wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.39.zip; unzip Trimmomatic-*``  
Or download and unzit the latest binairy and unzip your self at:  
http://www.usadellab.org/cms/?page=trimmomatic  
  
## Install python    
#### debian and ubunutu (apt): 
``sudo apt-get install python3``  

## isntall conda
https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html

## install snakemake
````
conda install -c conda-forge mamba  
mamba create -c conda-forge -c bioconda -n snakemake snakemake
````

#### Windows and others:  
https://www.python.org/downloads/  

## Install virtualenv  
``pip3 install virtualenv``

## install the pipeline
Clone this repo (requires git):  
`` git clone https://jjvalkema@bitbucket.org/jjvalkema/thema11_savethebees.git ``   
Browse to the pipeline folder:   
``cd thema11_savethebees`` 

activate the virtual environment:   
``source venv/bin/activate``   
  
<!--  comment  
snakemake and etc is in the venv
nanopolish is not used for now
  
``
git clone --recursive https://github.com/jts/nanopolish.git    
cd nanopolish    
make  
``  

-->

<!-- maybe yaml is better so we can have comments -->  
Configure the input and output file paths in: ``thema11_savethebees/pipeline/config.json``.    
Make sure to set all the values under input and output between the quotes (```"""```).  


## Run the the pipe line ####
Then browse to the pipeline folder: `` cd thema11_savethebees/pipeline``    

activate the virtual environment: ``conda activate snakemake`` 
 
And finally run the pipeline with snake make: ``snakemake -s snakemake_pipeline.smk --cores 4``   

### extra options
Add ``-r create_consensus`` to only create the consensus,  
``-r fast5_parsing`` to only extract the fast5 reads,   
``-r merge_single_fast5_folder`` extract fast5 reads and convert them to single reads (not done other wise)   
or ``-r illumina_processing`` to only do illumina data trimming and assembly.  

create dag picture: ``snakemake -s snakemake_pipeline.smk --cores 4 --dag | dot -Tsvg > dag.svg``
 
TODO recoures and credits ex: https://gist.github.com/nathanhaigh/3521724