# This file may be used to create an environment using:
# $ conda create --name <env> --file <this file>
# platform: linux-64
_libgcc_mutex=0.1=main
aioeasywebdav=2.4.0=py37_1000
aiohttp=3.6.2=py37h516909a_0
appdirs=1.4.3=py_1
argtable2=2.13=h14c3975_1001
async-timeout=3.0.1=py_1000
attrs=19.3.0=py_0
bcrypt=3.1.7=py37h8f50634_1
biopython=1.76=py37h516909a_0
boto3=1.13.17=pyh9f0ad1d_0
botocore=1.16.17=pyh9f0ad1d_0
brotlipy=0.7.0=py37h8f50634_1000
bwa=0.7.17=hed695b0_7
bzip2=1.0.8=h516909a_2
ca-certificates=2020.4.5.2=hecda079_0
cachetools=4.1.0=py_1
cairo=1.16.0=h18b612c_1001
certifi=2020.4.5.2=py37hc8dfbb8_0
cffi=1.14.0=py37hd463f26_0
chardet=3.0.4=py37hc8dfbb8_1006
clustalo=1.2.4=hfc679d8_2
cmake=3.17.0=h28c56e5_0
configargparse=1.2.3=pyh9f0ad1d_0
crc32c=2.0=py37h516909a_0
cryptography=2.9.2=py37hb09aad4_0
cudatoolkit=10.2.89=hfd86e86_1
curl=7.69.1=h33f0ec9_0
cycler=0.10.0=py_2
datrie=0.8.2=py37h8f50634_0
dbus=1.13.6=he372182_0
decorator=4.4.2=py_0
docutils=0.15.2=py37_0
dropbox=9.4.0=py_0
eigen=3.3.7=hc9558a2_1001
emboss=6.6.0=heeff6be_2
expat=2.2.9=he1b5a44_2
fastqc=0.11.9=0
fftw=3.3.8=nompi_h7f3a6c3_1110
filechunkio=1.8=py_2
font-ttf-dejavu-sans-mono=2.37=hab24e00_0
fontconfig=2.13.1=he4413a7_1000
freetype=2.10.2=he06d7ca_0
ftputil=3.4=py_0
gettext=0.19.8.1=hc5be6a0_1002
ghostscript=9.18=1
giflib=5.1.9=h516909a_0
gitdb=4.0.5=py_0
gitpython=3.1.2=py_0
glib=2.64.3=h6f030ca_0
google-api-core=1.17.0=py37hc8dfbb8_0
google-api-python-client=1.8.4=pyh9f0ad1d_0
google-auth=1.14.3=pyh9f0ad1d_0
google-auth-httplib2=0.0.3=py_3
google-cloud-core=1.3.0=py_0
google-cloud-storage=1.28.1=pyh9f0ad1d_0
google-resumable-media=0.5.0=py_1
googleapis-common-protos=1.51.0=py37hc8dfbb8_2
graphite2=1.3.13=he1b5a44_1001
graphviz=2.38.0=hf68f40c_1011
gst-plugins-base=1.14.5=h0935bb2_2
gstreamer=1.14.5=h36ae1b5_2
h5py=2.8.0=py37h39dcb92_0
harfbuzz=2.4.0=h37c48d4_1
hdf5=1.8.18=3
htslib=1.9=ha228f0b_7
httplib2=0.18.1=pyh9f0ad1d_0
icu=58.2=hf484d3e_1000
idna=2.9=py_1
imagemagick=7.0.8_11=pl526hc610aec_0
importlib-metadata=1.6.0=py37hc8dfbb8_0
importlib_metadata=1.6.0=0
intel-openmp=2020.1=217
ipython_genutils=0.2.0=py_1
jbig=2.1=h516909a_2002
jinja2=2.11.2=pyh9f0ad1d_0
jmespath=0.10.0=pyh9f0ad1d_0
jpeg=9c=h14c3975_1001
jsonschema=3.2.0=py37hc8dfbb8_1
jupyter_core=4.6.3=py37hc8dfbb8_1
kiwisolver=1.2.0=py37h99015e2_0
krb5=1.17.1=h173b8e3_0
ld_impl_linux-64=2.33.1=h53a641e_7
libblas=3.8.0=14_openblas
libcblas=3.8.0=14_openblas
libcurl=7.69.1=hf7181ac_0
libdeflate=1.0=h14c3975_1
libedit=3.1.20181209=hc058e9b_0
libffi=3.2.1=hd88cf55_4
libgcc=7.2.0=h69d50b8_2
libgcc-ng=9.1.0=hdf63c60_0
libgd=2.2.5=h5400f36_4
libgfortran=3.0.0=1
libgfortran-ng=7.5.0=hdf63c60_6
libiconv=1.15=h516909a_1006
liblapack=3.8.0=14_openblas
libopenblas=0.3.7=h5ec1e0e_6
libpng=1.6.37=hed695b0_1
libprotobuf=3.12.1=h8b12597_0
libssh2=1.9.0=hab1572f_2
libstdcxx-ng=9.1.0=hdf63c60_0
libtiff=4.1.0=hc3755c2_3
libtool=2.4.6=h14c3975_1002
libuuid=2.32.1=h14c3975_1000
libuv=1.38.0=h516909a_0
libwebp=0.5.2=7
libxcb=1.13=h14c3975_1002
libxml2=2.9.9=hea5a465_1
llvm-openmp=8.0.1=hc9558a2_0
lz4-c=1.9.2=he1b5a44_1
make=4.2.1=h1bed415_1
markupsafe=1.1.1=py37h8f50634_1
matplotlib=3.1.3=py37_0
matplotlib-base=3.1.3=py37hef1b27d_0
minimap2=2.17=h8b12597_1
mkl=2020.1=217
multidict=4.7.5=py37h8f50634_1
nanoget=1.13.0=py_0
nanomath=0.23.2=py_0
nanoplot=1.30.0=py_0
nanopolish=0.13.2=ha077697_0
nbformat=5.0.6=py_0
ncurses=6.1=hf484d3e_1002
networkx=2.4=py_1
ninja=1.10.0=hc9558a2_0
numpy=1.18.4=py37h8960a57_0
oauth2client=4.1.3=py_0
olefile=0.46=py_0
ont-fast5-api=3.1.3=py_0
openjdk=11.0.1=h516909a_1016
openjpeg=2.3.1=h981e76c_3
openmp=8.0.1=0
openssl=1.1.1g=h516909a_0
packaging=20.4=pyh9f0ad1d_0
pandas=1.0.3=py37h0da4684_1
pango=1.40.14=he7ab937_1005
paramiko=2.7.1=py37_0
patsy=0.5.1=py_0
pauvre=0.1.86=py_1
pcre=8.44=he1b5a44_0
perl=5.26.2=h516909a_1006
pillow=7.1.2=py37hb39fc2d_0
pip=20.0.2=py37_3
pixman=0.38.0=h516909a_1003
pkg-config=0.29.2=h516909a_1006
plotly=4.8.0=pyh9f0ad1d_0
prettytable=0.7.2=py_3
progressbar33=2.4=py_0
protobuf=3.12.1=py37h3340039_0
psutil=5.7.0=py37h8f50634_1
pthread-stubs=0.4=h14c3975_1001
pyasn1=0.4.8=py_0
pyasn1-modules=0.2.7=py_0
pycparser=2.20=py_0
pygments=2.6.1=py_0
pygraphviz=1.5=py37h8f50634_1002
pynacl=1.3.0=py37h516909a_1001
pyopenssl=19.1.0=py_1
pyparsing=2.4.7=pyh9f0ad1d_0
pyqt=5.9.2=py37hcca6a23_4
pyrsistent=0.16.0=py37h8f50634_0
pysam=0.15.3=py37hda2845c_1
pysftp=0.2.9=py_1
pysocks=1.7.1=py37hc8dfbb8_1
python=3.7.6=h0371630_2
python-dateutil=2.8.1=py_0
python-irodsclient=0.8.2=py_0
python_abi=3.7=1_cp37m
pytz=2020.1=pyh9f0ad1d_0
pyyaml=5.3.1=py37h8f50634_0
qt=5.9.7=h52cfd70_2
ratelimiter=1.2.0=py37_1000
readline=7.0=h7b6447c_5
requests=2.23.0=pyh8c360ce_2
retrying=1.3.3=py_2
rhash=1.3.6=h14c3975_1001
rsa=4.0=py_0
s3transfer=0.3.3=py37hc8dfbb8_1
samtools=1.9=h10a08f8_12
scipy=1.4.1=py37ha3d9a3c_3
seaborn=0.10.1=py_0
setuptools=46.4.0=py37_0
simplejson=3.17.0=py37h8f50634_1
sip=4.19.8=py37hf484d3e_0
six=1.15.0=pyh9f0ad1d_0
slacker=0.14.0=py_0
smmap=3.0.4=pyh9f0ad1d_0
snakemake=5.18.1=0
snakemake-minimal=5.18.1=py_0
sqlite=3.31.1=h7b6447c_0
statsmodels=0.11.1=py37h8f50634_1
tk=8.6.8=hbc83047_0
toposort=1.5=py_3
tornado=6.0.4=py37h8f50634_1
traitlets=4.3.3=py37hc8dfbb8_1
trimmomatic=0.39=1
unzip=6.0=h516909a_1
uritemplate=3.0.1=py_0
urllib3=1.25.9=py_0
vim=8.2.0770=py37he9c8336_0
virtualenv=16.7.5=py_0
wheel=0.34.2=py37_0
wrapt=1.12.1=py37h8f50634_1
xmlrunner=1.7.7=py_0
xorg-kbproto=1.0.7=h14c3975_1002
xorg-libice=1.0.10=h516909a_0
xorg-libsm=1.2.3=h84519dc_1000
xorg-libx11=1.6.9=h516909a_0
xorg-libxau=1.0.9=h14c3975_0
xorg-libxdmcp=1.1.3=h516909a_0
xorg-libxext=1.3.4=h516909a_0
xorg-libxpm=3.5.13=h516909a_0
xorg-libxrender=0.9.10=h516909a_1002
xorg-libxt=1.1.5=h516909a_1003
xorg-renderproto=0.11.1=h14c3975_1002
xorg-xextproto=7.3.0=h14c3975_1002
xorg-xproto=7.0.31=h14c3975_1007
xz=5.2.5=h7b6447c_0
yaml=0.2.4=h516909a_0
yarl=1.3.0=py37h516909a_1000
zipp=3.1.0=py_0
zlib=1.2.11=h7b6447c_3
zstd=1.4.4=h6597ccf_3
