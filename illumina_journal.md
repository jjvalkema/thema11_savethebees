test seq: https://trace.ncbi.nlm.nih.gov/Traces/sra/?view=search_seq_name&exp=SRX5250636&run=&m=search&s=seq
for when paired end reads break: https://github.com/linsalrob/fastq-pair
de-interleave fastq files: https://gist.github.com/nathanhaigh/3521724

## Install fastqc
#### debian and ubunutu (apt): 
``sudo apt-get install fastqc``
#### Other/universal:   
TODO
https://raw.githubusercontent.com/s-andrews/FastQC/master/INSTALL.txt

## install trimmomatic
Download the latest binairy
http://www.usadellab.org/cms/?page=trimmomatic
TODO  
``wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.39.zip; unzip Trimmomatic-*; cd Trimmomatic-0.39``

## install velveth    
TODO but already is on assemblix  
https://www.ebi.ac.uk/~zerbino/velvet/Manual.pdf  

## install SOAPdenovo
#### mega hit 
``conda install -c bioconda megahit``

##### SOAPdenovo main 
With apt  
``sudo apt-get install -y soapdenovo``  
With conda  
``conda install -c bioconda soapdenovo2``  
From source (errors)  
``git clone https://github.com/aquaskyline/SOAPdenovo2.git; SOAPdenovo2/make``  
Assemblix is already installed




## run trimmomatic  
````
java -jar /students/2019-2020/Thema11/minion_aj_jj/programs/Trimmomatic-0.39/trimmomatic-*.jar PE -phred33 \
/students/2019-2020/Thema11/minion_aj_jj/data/illumina_test/forward.fastq \
/students/2019-2020/Thema11/minion_aj_jj/data/illumina_test/reverse.fastq \
/students/2019-2020/Thema11/minion_aj_jj/data/illumina_test/forward_trimmed_paired.fastq \
/students/2019-2020/Thema11/minion_aj_jj/data/illumina_test/forward_trimmed_unpaired.fastq \
/students/2019-2020/Thema11/minion_aj_jj/data/illumina_test/reverse_trimmed_paired.fastq \
/students/2019-2020/Thema11/minion_aj_jj/data/illumina_test/reverse_trimmed_unpaired.fastq \
ILLUMINACLIP:/students/2019-2020/Thema11/minion_aj_jj/programs/Trimmomatic-0.39/adapters/TruSeq3-PE.fa:2:30:10:2:keepBothReads \
LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
````

## run fastqc
On raw reads:  
``fastqc /students/2019-2020/Thema11/minion_aj_jj/data/illumina_test_data_small.fastq``  
On trimmed reads:  
``/students/2019-2020/Thema11/minion_aj_jj/data/illumina_test_data_small_trimmed.fastq``

## run velveth  
``velveth Assem 31 -shortPaired -fastq -separate ../illumina_sequences/trimmed_reads_forward_paired.fastq \
../illumina_sequences/trimmed_reads_reverse_paired.fastq; velvetg Assem/ -ins_length 400 -exp_cov 21.3``

## important considerations for parameters form chapters at:  
https://www.ebi.ac.uk/~zerbino/velvet/Manual.pdf   
3.1.3 Multiple k-mers 
`` ./velveth output_directory/ m,M,s (..data files..)``  

5.2 Choice of hash length k  
tldr: between 10-20  
  
5.3 Choice of a coverage cutof  
check for errors in r to find a good cutoff  
See the pdf for details and script   

SOAPdenovo temp folder: ``/students/2019-2020/Thema11/minion_aj_jj/pipeline/illumina_assembly/SOAPdenovo``

## run SOAPdenovo
``soapdenovo2-63mer all -s SOAPdenovo_DWV_test_data.config -o outputGraph -p 80 -N 10135  ``

## run on new model basecall (TODO do need to be here?)
````
cat $(find $PWD /students/2019-2020/Thema11/minion_aj_jj/bees_data/ | egrep '\.fastq$' | tr '\n' ' ') > temp_all_reads.fastq
````