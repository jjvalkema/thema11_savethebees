configfile: "basecall_config.yml"

rule all:
    input:
        directory("results/Nanoplot_basecalls")

## Step 1: Basecall the reads
rule basecall:
    """Basecall the fast5 reads with the Guppy basecaller from Oxford Nanopore Technologies."""
    input:
        samples = config["samples"],   # fast5 sample dir
        id_list = config["read_ids"],   # Read ids to filter
        config = config["config_model"], # Guppy config
        model = config["basecall_model"], # Basecall model
        basecaller = config["basecaller"]  # Guppy basecaller
    params:
        scale = 9.88,   
        offset = -59.85
    output:
        dir = directory("basecalled_reads"), # output dir
    message:
        "Basecalling reads from: {input.samples} with model: {input.model}, qscale = {params.scale}, qoffset = {params.offset}. Ouput stored in output:{output.dir}"
    shell:
        "{input.basecaller} -i {input.samples} -s {output.dir} -c {input.config} -m {input.model} --qscore_offset {params.offset} --qscore_scale {params.scale} --device cuda:0 --read_id_list {input.id_list}"
         #/home/tools/ont-guppy/bin/guppy_basecaller -i /home/reads_test1 -s basecalls_test --qscore_offset {params.offset} --qscore_scale {params.scale} -c /home/tools/ont-guppy/data/rna_r9.4.1_70bps_hac.cfg --device cuda:0

# Step 2
rule quality_check:
    """Create NanoPlot plots before and after the training. These plots show the average read length and quality of the reads."""
    input:
        basecall_dir = rules.basecall.output.dir,# sample = [rules.basecall.output.dir, rules.basecall_with_new_model.output.dir]), # basecall summary file
        nanoplot = config["nanoplot"],
    output:
        directory("results/Nanoplot_basecalls"), # output dir
        #report(directory("results/Nanoplot_basecalls"), caption="report/somedata.rst", category="Step 3")
    message:
        "Checking the quality of the {input.basecall_dir} reads with nanoplot after with {input.nanoplot}"
    shell:
        "{input.nanoplot} --summary {input.basecall_dir}/sequencing_summary.txt --loglength -o {output}"
        #/lib/taiyaki/venv/bin/NanoPlot --summary basecalls_test/sequencing_summary.txt --loglength -o results/Nanoplot_output_testdata