import os
from ont_fast5_api.conversion_tools import fast5_subset,multi_to_single_fast5

configfile: "config/config.json"
#TODO move naming files to here maybe?
consensus_folder =  config["output"]["basefolder"]+os.sep+"consensus_sequences"
illumina_seqs_folder = config["output"]["basefolder"]+os.sep+"illumina_sequences"
illumina_assembly_folder = config["output"]["basefolder"]+os.sep+"illumina_assembly"
install_dir = os.getcwd()

rule all:
    input:
        #fast5 parsing
        config["output"]["basefolder"]+os.sep+"DWV_multi_reads_folder",
        # the single read are no longer needed since we don't use chiron
        config["output"]["basefolder"]+os.sep+"DWV_single_reads_folder_merged",
        # old model isn't used for now
        config["output"]["basefolder"]+os.sep+"old_model.fastq",

        # consensus:
        consensus_folder+os.sep+"DWV_reff_consensus_without_IUPAC.fasta",

        # illumina processing:
        illumina_seqs_folder + os.sep + "qc_reports",
        illumina_assembly_folder,

        basefolder = config["output"]["basefolder"]

    shell:
         # TODO remove the chmod to prevent security issues (keep it for convenience)
        "echo \"############ WARNING FOR DEVS ############ \n ↓ TODO REMOVE THIS FOR SECURITY ↓ \n "
        "finnished with processing giving permisions to all users for (might take a while): {input.basefolder} \"; \
        chmod a+rwx -R {input.basefolder}"

rule illumina_processing:
    input:
        illumina_seqs_folder + os.sep + "qc_reports",
        illumina_assembly_folder

rule fast5_parsing:
    input:
        config["output"]["basefolder"]+os.sep+"DWV_multi_reads_folder",
        #config["output"]["basefolder"]+os.sep+"DWV_single_reads_folder_merged"

rule create_consensus:
    input:
        consensus_folder+os.sep+"DWV_reff_consensus_without_IUPAC.fasta"

rule assemble_illumina_reads:
    threads: workflow.cores
    params:
        genome_size = config["extra"]["genome_size"]
    input:
        forward_paired_trimmed = illumina_seqs_folder+os.sep+"trimmed_reads_forward_paired.fastq",
        reverse_paired_trimmed = illumina_seqs_folder+os.sep+"trimmed_reads_reverse_paired.fastq",
        SOAPdenovo_config = config["extra"]["SOAPdenovo_config"]
    output:
        illumina_assembly_folder = illumina_assembly_folder
    shell:
        "mkdir {output.illumina_assembly_folder}; soapdenovo2-63mer all -s {input.SOAPdenovo_config} -o outputGraph -p {threads} -N {input.genome_size}"

rule create_qc_reports:
    threads: workflow.cores
    input:
        raw_reads = config["input"]["illumina_reads"],
        forward_paired_trimmed = illumina_seqs_folder+os.sep+"trimmed_reads_forward_paired.fastq",
        reverse_paired_trimmed = illumina_seqs_folder+os.sep+"trimmed_reads_reverse_paired.fastq"
    output:
        qc_reports_folder = directory(illumina_seqs_folder + os.sep + "qc_reports")
    shell:
         # TODO threads
        "mkdir {output.qc_reports_folder}; fastqc -t {threads} {input.raw_reads} {input.forward_paired_trimmed} {input.reverse_paired_trimmed} -o {output.qc_reports_folder}"

# rule trim_unpaired_illumina_reads:
#     input:
#         raw_reads = config["input"]["illumina_reads"],
#         trimmomatic = config["programs"]["trimmomatic_install_folder"] + os.sep
#     output:
#         # TODO give better name depending on input file name?
#         trimmed_reads = illumina_seqs_folder + os.sep + "trimmed_reads.fastq"
#     shell:
#         "java -jar {input.trimmomatic}trimmomatic-*.jar SE -phred33 {input.raw_reads} {output.trimmed_reads} ILLUMINACLIP:{input.trimmomatic}adapters/TruSeq3-SE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36"

rule trim_paired_illumina_reads:
    threads: workflow.cores * 0.5
    input:
         forward_reads =illumina_seqs_folder + os.sep + "raw_reads_forward.fastq",
         reverse_reads =illumina_seqs_folder + os.sep + "raw_reads_reverse.fastq",
         trimmomatic = config["programs"]["trimmomatic_install_folder"] + os.sep
    output:
        # TODO give better name depending on input file name?
        forward_paired = illumina_seqs_folder+os.sep+"trimmed_reads_forward_paired.fastq",
        forward_unpaired = illumina_seqs_folder+os.sep+"trimmed_reads_forward_unpaired.fastq",
        reverse_paired = illumina_seqs_folder+os.sep+"trimmed_reads_reverse_paired.fastq",
        reverse_unpaired = illumina_seqs_folder+os.sep+"trimmed_reads_reverse_unpaired.fastq",
    shell:
         "java -jar {input.trimmomatic}trimmomatic-*.jar PE -phred33 -threads {threads} \
         {input.forward_reads} {input.reverse_reads} \
         {output.forward_paired} {output.forward_unpaired} {output.reverse_paired} {output.reverse_unpaired} \
         ILLUMINACLIP:{input.trimmomatic}adapters/TruSeq3-PE.fa:2:30:10:2:keepBothReads \
         LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36"

rule deinterleave_illumina_reads:
    input:
        interleaves_reads = config["input"]["illumina_reads"],
    output:
        forward_reads =illumina_seqs_folder + os.sep + "raw_reads_forward.fastq",
        reverse_reads =illumina_seqs_folder + os.sep + "raw_reads_reverse.fastq",
    shell:
         "chmod +x ./../scripts/bash/deinterleave_fastq.sh; ./../scripts/bash/deinterleave_fastq.sh < {input.interleaves_reads} {output.forward_reads} {output.reverse_reads}"

rule convert_IUPAC_codes:
    input:
        DWV_reff_consensus_withIUPAC = consensus_folder +os.sep+"DWV_reff_consensus_with_IUPAC.fasta"
    output:
        DWV_reff_consensus = consensus_folder+os.sep+"DWV_reff_consensus_without_IUPAC.fasta"
    shell:
        'python3 ../scripts/python/IUPAC_converter.py -i {input.DWV_reff_consensus_withIUPAC} '
        '-o {output.DWV_reff_consensus}'

rule create_consensus_seq_on_reff:
    input:
        DWV_reff_MSA_fasta = consensus_folder +os.sep+"DWV_reff_MSA_fasta.msf"
    output:
        DWV_reff_consensus = consensus_folder +os.sep+"DWV_reff_consensus_with_IUPAC.fasta"
    shell:
        'consambig -sequence {input.DWV_reff_MSA_fasta} -outseq {output.DWV_reff_consensus} '
        '-name DWV_NCIBI_NL_DE_BE_11_seqs -snucleotide1 Y'
             #todo do naming based on input files

rule create_MSA_on_reff:
    threads: workflow.cores * 0.5
    input:
        DWV_reff_seqs = consensus_folder +os.sep+"DWV_reff_seqs_multifasta"
    output:
        DWV_reff_MSA_fasta = consensus_folder +os.sep+"DWV_reff_MSA_fasta.msf"
    shell:
        'clustalo -i {input.DWV_reff_seqs} --out {output.DWV_reff_MSA_fasta} --threads={threads} -v --outfmt=msf'

rule download_reff_seqs:
    output:
        DWV_reff_seqs_multifasta = consensus_folder +os.sep+ "DWV_reff_seqs_multifasta"
    shell:
        "wget {config[extra][DWV_reff_seqs_multifasta_download_link]} -O {output.DWV_reff_seqs_multifasta}"

rule create_folders:
    output:
        directory(consensus_folder)
    shell:
         "mkdir {consensus_folder})"


# rule create_test_ref_genome:
# #TODO do all reads
#     input:
#         DWV_single_reads_folder = config["output"]["basefolder"]+os.sep+"DWV_single_reads_folder" + "/0"
#     output:
#         DWV_test_reff_genome = config["output"]["basefolder"]["DWV_test_reff_genome"] + "batch0.fasta"
#     shell:
#         'poretools fasta {input.DWV_single_reads_folder} > {output.DWV_test_reff_genome}'

rule get_old_model_basecalls:
    input:
         DWV_reads_multi = config["output"]["basefolder"]+os.sep+"DWV_multi_reads_folder"
    output:
        old_model =  config["output"]["basefolder"]+os.sep+"old_model.fastq"
    shell:
        "python3 ../scripts/python/fastq_from_fast5.py -i {input.DWV_reads_multi} -o {output.old_model}"

rule merge_single_fast5_folder:
    input:
        DWV_reads_single = config["output"]["basefolder"]+os.sep+"DWV_single_reads_folder"
    output:
        DWV_reads_single_merged =  directory(config["output"]["basefolder"]+os.sep+"DWV_single_reads_folder_merged")
    shell:
        'python3 ../scripts/python/folder_combiner.py -p {input.DWV_reads_single} -o {output.DWV_reads_single_merged}; '
        'rm -f -R {input.DWV_reads_single}'


rule convert_to_single_fast5:
    threads: workflow.cores * 0.5
    input:
        DWV_reads_multi = config["output"]["basefolder"]+os.sep+"DWV_multi_reads_folder",
    output:
        DWV_reads_single = directory(config["output"]["basefolder"]+os.sep+"DWV_single_reads_folder")
    run:
         #TODO threads
        multi_to_single_fast5.batch_convert_multi_files_to_single(input_path=input.DWV_reads_multi,
                                                                  output_folder=output.DWV_reads_single, threads=threads,
                                                                  recursive=True, follow_symlinks=False)

rule get_DWV_reads:
    threads: workflow.cores * 0.5
    input:
        raw_fast5_folder= config["input"]["raw_fast5_folder"],
        read_ids=config["input"]["read_ids"],
    output:
        DWV_reads_multi = directory(config["output"]["basefolder"]+os.sep+"DWV_multi_reads_folder")
    run:
        multifilter = fast5_subset.Fast5Filter(input_folder=input.raw_fast5_folder,read_list_file=input.read_ids,
                                               output_folder=output.DWV_reads_multi,batch_size=4000,threads=threads,
                                               recursive=True,file_list_file="",follow_symlinks=False)
        multifilter.run_batch()

rule make_basefolder:
    input:
         basefolder = config["output"]["basefolder"]
    shell:
         "echo \"The output folder: {input.basefolder} doesn't exists creating it now\"; mkdir {input.basefolder}"