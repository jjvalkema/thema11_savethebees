#!/usr/bin/env python
import sys
from argparse import ArgumentParser
import os.path
from os import path
import shutil


# todo docstring and clean up
class folder_combiner:
    def __init__(self, parent_folder_path, output_folder_path):
        # TODO clearer error handeling for the user on these assertions
        assert path.exists(parent_folder_path), "Parent folder: {} doesn't exists!".format(parent_folder_path)
        assert not path.exists(output_folder_path), "output folder: {} already exists!".format(output_folder_path)
        self.parent_folder_path = parent_folder_path
        self.output_folder_path = output_folder_path
        os.mkdir(output_folder_path)

    def merge(self):
        # TODO remove the parent folder and move the txt file over as well
        for child_folder in os.listdir(self.parent_folder_path):
            if not (child_folder.endswith(".txt") or child_folder.startswith(".")):
                child_folder_files = os.listdir("{}/{}".format(self.parent_folder_path, child_folder))
                print("moving {} files from subdir {} to {}: ".format(
                    len(child_folder_files),
                    child_folder,
                    self.output_folder_path
                ))
                for file in child_folder_files:
                    shutil.move("{}/{}/{}".format(self.parent_folder_path, child_folder, file),
                                "{}/{}".format(self.output_folder_path, file))


def main():
    parser = ArgumentParser("Tool for merging folder together")
    parser.add_argument('-p', '--parent_folder', required=True,
                        help="Path to parent folder")
    parser.add_argument('-o', '--output_folder', required=True,
                        help="Path to output folder")
    args = parser.parse_args()

    combiner = folder_combiner(args.parent_folder, args.output_folder)
    combiner.merge()


if __name__ == '__main__':
    main()
